package com.udacity.gradle.builditbigger;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Joke {
    private List<String> mJokesList = new ArrayList<String>();

    public String getJoke(){
        mJokesList.add("Q: Why do they never serve beer at a math party?\nA: Because you can't drink and derive...");
        mJokesList.add("Q: Why did the chicken cross the Möbius strip?\nA: To get to the same side.");
        mJokesList.add("Q: How does a mathematician propose to his girlfriend?\nA: With a polynomial ring.");
        mJokesList.add("Rene Descartes goes into a bar late one night for a beer. At closing time, the bartender makes Last Call and asks him, \"Get you another?\" Descartes replies, \"I think not.\" And disappears.");
        mJokesList.add("A SQL query goes into a bar, walks up to two tables and asks, \"Can I join you?\"");

        int index = new Random().nextInt(mJokesList.size());

        return mJokesList.get(index);
    }
}
