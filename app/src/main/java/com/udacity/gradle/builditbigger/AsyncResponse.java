package com.udacity.gradle.builditbigger;

/**
 * Created by Kourosh on 2/4/2016.
 */
public interface AsyncResponse {
    void processFinish(String output);
}
