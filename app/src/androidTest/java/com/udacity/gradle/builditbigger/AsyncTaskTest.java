package com.udacity.gradle.builditbigger;

import android.os.ConditionVariable;
import android.support.v7.app.AppCompatActivity;
import android.test.AndroidTestCase;
import android.util.Log;

/**
 * Created by Kourosh on 2/5/2016.
 */
public class AsyncTaskTest extends AndroidTestCase {
    private AsyncTestActivity activity;
    private ConditionVariable cv;

    public AsyncTaskTest() {

    }

    @Override
    protected void setUp() throws Exception {
        cv = new ConditionVariable();
        activity = new AsyncTestActivity();
        activity.createAsyncTask();
        cv.block(20000L);
    }

    @Override
    protected void tearDown() throws Exception {
    }

    public class AsyncTestActivity extends AppCompatActivity implements AsyncResponse {
        private EndpointsAsyncTask mAsyncTask;
        private static final String TAG = "AsyncTestActivity";

        public void createAsyncTask() {
            mAsyncTask = new EndpointsAsyncTask();
            mAsyncTask.delegate = this;
            mAsyncTask.execute();
        }


        @Override
        public void processFinish(String output) {
            Log.v(TAG, "The retrieved joke: " + output);
            assertFalse(output.isEmpty());
            cv.open();
        }
    }
}
